# README #

A simple tool to automatically cut out unnecessary parts of a video

### What is this repository for? ###

* You have videos with a lot of repeated frames that you wish to remove
* You want to keep the parts with a large delta of pixel changes
* You are tired of manually cutting/ editing your video

### How do I get set up? ###

* Install node-fluent-ffmpeg (https://github.com/fluent-ffmpeg/node-fluent-ffmpeg)
* Install node.js
* Clone the project
* Add your video to the same directory as index.js
* Change the name and filter options 
* Once you are ready, run `npm start`
