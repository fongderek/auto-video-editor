var ffmpeg = require('fluent-ffmpeg');
ffmpeg('original_video.mp4')
    .videoFilters([
        {
            filter: 'decimate',
            options: { cycle: 2, dupthresh: 2, scthresh: 4 },
        },
        'setpts=N/FRAME_RATE/TB'
    ])
    // .duration(455)
    // duration is a hardcoded number right now
    .save('edited_video.mp4');

